import React from "react";
import "./App.css";
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import firebase from "firebase";
import UserProvider from "./providers/UserProvider";
import { Application } from "./Components/Application";

function App() {
  return (
    <UserProvider>
      <Application />
    </UserProvider>
  );
}

export default App;
