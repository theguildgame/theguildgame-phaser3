import React, { useContext } from "react";
import { Router } from "@reach/router";
import { SignIn } from "./SignIn";
import ProfilePage from "./ProfilePage";
import { UserContext } from "../providers/UserProvider";

export const Application = () => {
  const { user } = useContext(UserContext);
  console.log(user);
  return user ? (
    <ProfilePage />
  ) : (
    <Router>
      <SignIn path="/" />
    </Router>
  );
};
