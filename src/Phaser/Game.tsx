import Phaser from "phaser";
import { useEffect } from "react"
import { gameConfig } from "./gameConfig";


function  Game () {

  useEffect(() => {
    new Phaser.Game(gameConfig);
  })

  return <div id="theguildgame-container"></div>
}

export default Game