enum TextureKeys {
  Background = "background",
  RocketMouse = "rocket-mouse",
  MouseHole = "mouse-hole",
  Window1 = "window1",
  Window2 = "window2",
}

export default TextureKeys;
