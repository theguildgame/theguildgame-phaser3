import Phaser from "phaser";
import AnimationKeys from "../consts/AnimationKeys";
import TextureKeys from "../consts/TetureKeys";

export default class RocketMouse extends Phaser.GameObjects.Container {
  private readonly flames: Phaser.GameObjects.Sprite;
  private readonly cursors: Phaser.Types.Input.Keyboard.CursorKeys;
  body!: Phaser.Physics.Arcade.Body;

  constructor(scene: Phaser.Scene, x: number, y: number) {
    super(scene, x, y);

    const mouse = scene.add
      .sprite(0, 0, TextureKeys.RocketMouse)
      .setOrigin(0.5, 1)
      .play(AnimationKeys.RocketMouseRun);

    this.flames = scene.add
      .sprite(-63, -15, TextureKeys.RocketMouse)
      .play(AnimationKeys.RocketFlamesOn);

    this.enableJetpack(false);

    this.add(this.flames);
    this.add(mouse);

    scene.physics.add.existing(this);

    this.body.setSize(mouse.width, mouse.height);
    this.body.setOffset(mouse.width * -0.5, -mouse.height);

    this.cursors = scene.input.keyboard.createCursorKeys();
  }

  enableJetpack(enabled: boolean) {
    this.flames.setVisible(enabled);
  }

  preUpdate() {
    if (this.cursors.space?.isDown ?? true) {
      this.body.setAcceleration(0, -600);
      this.enableJetpack(true);
    } else {
      this.body.setAcceleration(0, 0);
      this.enableJetpack(false);
    }
  }
}
