import Phaser from "phaser";
import Preloader from "./scenes/Preloader";
import TheGuildGame from "./scenes/TheGuildGame";

export const gameConfig: Phaser.Types.Core.GameConfig = {
  parent: "theguildgame-container",  
  type: Phaser.AUTO,
  width: 800,
  height: 640,
  physics: {
    default: "arcade",
    arcade: {
      gravity: { y: 200 },
      debug: true,
    },
  },
  scene: [Preloader, TheGuildGame],
};


