/* eslint-disable react/prop-types */
import React, {
  // Component,
  createContext,
  FunctionComponent,
  useEffect,
  useState,
} from "react";
import { auth } from "../services/firebase";
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import firebase from "firebase";

export const UserContext = createContext<{
  user: firebase.User | null;
}>({ user: null });

export const UserProvider: FunctionComponent = (props) => {
  const [user, setUser] = useState<{ user: firebase.User | null }>({
    user: null,
  });

  useEffect(() => {
    auth.onAuthStateChanged((userAuth) => {
      setUser({ user: userAuth });
    });
  });

  return (
    <UserContext.Provider value={user}>{props.children}</UserContext.Provider>
  );
};

export default UserProvider;
