
import firebase from 'firebase';
import { firebaseConfig } from '../config/firebase';


firebase.initializeApp(firebaseConfig);
firebase.analytics();

export const auth = firebase.auth();
export const googleProvider = new firebase.auth.GoogleAuthProvider()

export const signInWithGoogle = () => {

  return auth.signInWithPopup(googleProvider)
}